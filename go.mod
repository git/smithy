module github.com/honza/smithy

go 1.16

require (
	github.com/AaronO/go-git-http v0.0.0-20161214145340-1d9485b3a98f
	github.com/alecthomas/chroma v0.8.2
	github.com/bytesparadise/libasciidoc v0.6.0
	github.com/gin-gonic/gin v1.6.3
	github.com/go-git/go-git/v5 v5.1.0
	github.com/rakyll/statik v0.1.7
	github.com/spf13/cobra v1.1.1
	github.com/yuin/goldmark v1.2.1
	github.com/yuin/goldmark-highlighting v0.0.0-20200307114337-60d527fdb691
	gopkg.in/yaml.v2 v2.3.0
)
